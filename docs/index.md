# Welcome to the LibreCube Documentation!

At LibreCube our vision is to enable everyone to get involved in building systems for exploration of near and remote locations using open source hardware and software. We believe that discovering new worlds and getting scientific insights should be a open to everyone.

Use the navigation menu to browse through the documentation.

The official LibreCube website is here: [https://librecube.org](https://librecube.org)

If you like to contribute to this documentation, go to the [source code](https://gitlab.com/librecube/librecube.gitlab.io).

All documentation is licensed as [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)
