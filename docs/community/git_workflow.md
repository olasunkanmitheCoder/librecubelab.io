# Git Workflow

Provided here is the typical workflow we use for LibreCube projects.

## Set up your local repositroy

If the project you want to work on does not already exist (ie. a new prototype),
please inform the admin to create such an empty project. Then follow these steps.

1. Clone the LibreCube repository to your computer: `git clone <path_to_librecube_repo>`.
2. Create a new branch (see below).
3. Do your work.
4. When ready, inform the project admin(s) to review your work and to merge it into the `main` branch.

## Branching

The `main` branch contains the latest version of the project and is tested and runnable.

Urgent fixes may be applied directly to `main`.

The `develop` branch is taken from `main` and typically a bit ahead of `main`.
It is also tested and runnable and will be merged back into `main` at some point.

Think of the `develop` branch as being used for sprints. At the begin of a sprint,
feature and fixes branches are based off from `develop`. Eventually, they will
be merged into `develop` when completed.

The naming of your branch shall be `issue-XX`, using the number of the relevant
issue in the GitLab repository.

![](assets/git_branches.png)

## Commits

Write your commit messages in present tense and start with the commit type.
Add a semicolon ";" if there are more than one sentence.

- add: The new feature you're adding to a particular application
- mod: A modification to the code that changes functionality
- fix: A bug fix or similar
- refactor: Code improvements that do remain same functionality
- test: Everything related to testing
- docs: Everything related to documentation
- minor: For really minimal changes

Example:

```
[fix] wrong type conversion of sender's .emit() method
[add] parameter `dist` to Entity class constructor
[test] simulate loss of frames during UDP transport; extend unit test cases
```

For minor changes, a simple comment `minor` is sufficient.

## Rebasing

When working on your issue branch the develop branch may evolve due to other features and fixes being merged into develop. To take those changes into your branch use the git rebasing functionality. You may apply this every time before starting working on your branch. The typical workflow is as follows:

- Checkout develop:

```
$ git checkout develop
```

- Make sure develop is up to date:

```
$ git pull
```

- Checkout your branch:

```
$ git checkout issue-01
```

- Rebase with develop:

```
$ git rebase develop
```

- Alternatively, run the rebasing in an interactive way:

```
$ git rebase -i develop
```

- If merge issues are encountered (because conflicting changes were done in same files) you have to resolve them. Most IDEs provide a merge tools for this. Or use the command line:

```
$ git mergetool
```

- After fixing merge issues (if any) continue with rebase:

```
$ git rebase --continue
```

- If you make a mistake while merging, abert the merge to discard all changes you've made:

```
$ git rebase --abort
```

- Finally push changes to the remote repository. You need to force the push, since the commit history of the remote feature branch will be altered:

```
$ git push -f
```

## Merging

After completion of your issue branch, raise a merge request to have it
merged into `develop`, after being reviewed by one of the project owners.
