# Contributing

So you are interested in contributing to LibreCube? That's great!
LibreCube projects are community-maintained projects and we appreciate
contributions. Everyone is welcome to join the LibreCube community, there is
no formalism attached to it. Simply start contributing!

What to work on? Have a look in particular at the [prototype projects](https://gitlab.com/librecube/prototypes), there usually are several open issues to work on.
Or check out the several LibreCube communication channels to learn what needs to be done. 
Surely, you may also propose new projects.

Once you identified what to work on, follow our [Git Workflow](git_workflow.md)
to implement your contributions.

Please also consider our [Coding Style](coding_style.md) when writing code.
