# Reference Architecture

## Top Level Architecture

The Reference Architecture (REFA) provides the framework to all LibreCube projects.
It ensures compatible interfaces, coherent designs and a modular architecture.

A typical mission scenario consists of the top level segments shown here:

![](assets/top_level_segments.png)

The [Remote Segment](remote_segment/index.md) is the satellite, drone, rover, or
other vehicle(s) that carry out the mission goals. The mission goals could be
defined as taking images of Earth, collecting samples from unchartered places,
or delivering supplies to remote locations. The LibreCube REFA defines modular
system elements, such as power modules, structures, and communication systems
that can be (re)used to build various kinds of vehicles.

The [Ground Segment](ground_segment/index.md) does the monitoring and control of
the remote assets, in accordance with the mission goals. It comprises all the
elements for interfacing to the remote segment, processing of information, and
data distribution. The LibreCube REFA defines the building blocks for typical
ground applications.

The **User Segment** receives the captured mission data for
further science processing and distribution to the public, for example.
This part is usually very mission specific and thus not covered a lot by the
LibreCube REFA.

Not included in this figure is the **Launch Segment**, which is the system element
that carries the vehicle(s) to its remote location. For satellites this would
be the launch rocket. LibreCube does not encompass the Launch Segment
in the REFA (yet).

The system elements of the REFA can be structured in a hierarchical tree:

    Remote Segment
        - Instruments
            - Remote Sensing
            - ...
        - Platform
            - Structure and Mechanisms
            - Power
            - Communication
            - Thermal
            - Command and Data Handling
            - Attitude and Orbit Control
            - Propulsion
        - Support Equipment
            - Electrical Ground Support Equipment (EGSE)
            - Mechanical Ground Support Equipment (MGSE)
    Ground Segment
        - Ground Station System
            - Antenna System
            - Station System
            - Network System
        - Mission Operations System
            - Mission Control System
            - Mission Planning System
            - Automation System
            - Mission Support
            - Data Archive and Dissemination
            - Simulator
    User Segment

Note that the definition of the reference architecture is still work in progress.


## Protocols

The protocol stacks for interfacing the top level architecture elements is
described here.

### Remote Segment <> Ground Station System

The elements of the remote segment (such as satellites) interface to the ground
station elements of the ground segment using this protocol stack:

| Layer | Protocol
|-|-
| Application Layer | [ECSS PUS](https://ecss.nl/standard/ecss-e-st-70-41c-space-engineering-telemetry-and-telecommand-packet-utilization-15-april-2016/) for monitoring and control / [CCSDS CFDP](https://public.ccsds.org/Publications/SIS.aspx) for file delivery
| Transport Layer | (not applicable)
| Network Layer | [Space Packet Protocol](https://public.ccsds.org/Publications/SLS.aspx)
| Data Link Layer | [TC/TM Space Data Link Protocol & TC/TM Sync. and Channel Coding](https://public.ccsds.org/Publications/SLS.aspx)
| Physical Layer | Radio Frequency and Modulation

### Ground Station System <> Mission Operations System

The ground stations do interface with the mission operations system using the
[SLE protocol](https://public.ccsds.org/Publications/CSS.aspx), making use of following SLE services:

- Forward communications link transmission unit (FCLTU): provides CLTUs for uplink to the spacecraft
- Return all frames (RAF): provides the telemetry frames from a single space link symbol stream
- Return channel frames (RCF): provides master channel (MC) or specific virtual channels (VC)
- Return operational control field (ROCF): provides MC or VC operational control fields (OCFs)
