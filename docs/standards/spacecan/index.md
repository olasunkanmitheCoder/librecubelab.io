# SpaceCAN

The [CAN bus](https://de.wikipedia.org/wiki/Controller_Area_Network) is a robust vehicle bus standard designed to allow microcontrollers and devices to communicate with each other. It is a message-based protocol, designed originally for multiplex electrical wiring within automobiles and is also heavily used in robotics and aerospace.

CAN has many years of heritage and is also qualified for space use. In particular, [ECSS](https://ecss.nl) has developed the CANbus Extenstion Protocol Standard [ECSS-E-ST-50-15C](assets/ECSS-E-ST-50-15C.pdf) specifically for spacecraft projects that opt to use the CAN Network for on-board communications and control. It also defines the optional use of the CANopen standard as an application layer protocol operating in conjunction with the CAN Network data link layer.

For LibreCube's purpose however, the ECSS CANbus is deemed too complex in terms of implementation and usage. Thus we have further modified the ECSS CANbus Standard to fit the needs of typical small spacecraft (and rovers, drones, etc.), while being easy to use and implement. This is what we call the **SpaceCAN** bus.

We consider that the SpaceCAN bus is used for control and monitoring, allowing the exchange of commands and telemetry among spacecraft subsystems. Typically, a central processing unit is commanding other intelligent nodes (such as the power system, communication system, and payloads) and collects status information from them. The data to be exchanged on this bus is of moderate volume but must be transmitted in a reliable way. The bus us not intended to route high-volume data, such as science data, but to ensure reliable and robust communication between controller and responder nodes of the network. For this, small messages are sent between nodes that must arrive without error and with very little delay.

## Topology

The system bus is designed as a linear bus and is composed of a nominal and redundant chain (bus A and bus B).

![](assets/topology.png)

The controller node can talk to responders and the responders can talk to the controller. The responders **do not** talk with each other. If data needs to be transferred from one responder to the other, this must be coordinated by and go through the controller. The concept behind this architecture is that of a central computer that manages the satellite and which is connected to intelligent subsystems (i.e. that have their own microcontrollers). The interconnection of the controller and the various responder nodes form a network.

The network is thus composed of a single controller (with node ID 0) and up to 127 responders (with node ID 1 to 127). The node IDs are typically hard-coded in software and do not change during operation. Node IDs with lower value have higher priority in communication. That means, critical systems must be given lower IDs.

## Bus Architecture

Nodes can connect to the bus via selective or parallel access.

The selective bus access architecture implements a single CAN controller and interfaces the redundant CAN network via two transceivers. A bus selection mechanism is implemented between the CAN controller and the transceivers allowing the application to select the bus to be used for communication.

The parallel bus access architecture interfaces the redundant CAN network through a pair of CAN controllers.

| ![](assets/selective_bus_access_architecture.png) | ![](assets/parallel_bus_access_architecture.png)
|-|-
|Selective Bus Access|Parallel Bus Access

## CAN-ID Format

The CAN data frames carry an 11-bit field for the CAN ID which identify the message and provide a priority scheme (lower CAN IDs have higher priority). ECSS-CAN, which is based on CANopen, splits the CAN ID into a 4-bit function code (to identify the service) and a 7-bit node ID address. The function code together with the node ID then forms a communication object.

| Object            | CAN ID (hex) | Originator
|-|-|-
| Heartbeat         | 700           | Controller
| Sync              | 080           | Controller
| SCET Time         | 180           | Controller
| UTC Time          | 200           | Controller
| Telecommand (TC)  | 280 + Node ID | Controller
| Telemetry (TM)    | 300 + Node ID | Responder

## Redundancy Management via Heartbeats

The system bus is made resilient to single point failure (such as problem in cabling or connector fault) through redundant physical media topology. Redundant communication channels require a redundancy management scheme. The selected scheme for cold redundancy (that is, one bus active at a time) applies the concept of node monitoring via heartbeat frames.

The controller node defines the bus to be considered active by periodic transmission of heartbeat frames (CAN ID = 0x700, no data) on the active bus. The heartbeat period is typically in the range of several hundred milliseconds. The controller can switch over and operate on the alternate bus by stopping transmission of the heartbeat messages on the active bus and starting them on the alternate bus, which then becomes the active bus.

The responder nodes monitor the presence of the heartbeat from the controller to determine the active bus. It follows this logic:

- When a responder node misses the controller heartbeat for a defined number of times (`max HB miss`), it shall switch to the alternate bus and listen there for heartbeats.
- If it detects a heartbeat on that bus, it shall consider this one as the active bus.
- If no heartbeat is received after `max HB miss` times, it shall switch again to the other bus.
- This bus toggling shall be continued for a predefined number of times (`max bus switch`) or for infinite time.

This way, the responder nodes will try to find the controller heartbeats and when found, stay on the active bus.

The decision on when the controller node initiates a bus switch is application specific and not prescribed here. Typically, when responder nodes do not respond to controller commands the controller node may try a bus switch-over to detect if a bus communication problems exists.

## Synchronization

Synchronous network behavior is achieved with the SYNC protocol. The controller node periodically transmits SYNC frames (CAN ID = 0x080, no data) to indicate to the responders to start their application-specific behavior. This could trigger for example the initiation of measurements or the sending of telemetry to the controller node. The SYNC period is usually in the range of a few seconds.

## Time Distribution

Typically, the central on-board computer manages the spacecraft time. In addition, other systems on the spacecraft may also maintain a local time (for example an attitude control system with its own processing unit). The time distribution protocol specified here distributes a sample of the central time to devices maintaining a local time. When and how often the central time is distributed to time consumers is application specific.

**SCET Format**

The controller node shall maintain time information using spacecraft elapsed time (SCET) as defined in clause 3.2 of [CCSDS 301.0-B-4](assets/301x0b4e1.pdf). The time code format of the SCET is the CCSDS unsegmented time code (CUC): an binary count of seconds and binary power of sub-seconds. The SCET is thus a free running counter of up to 232 seconds (coarse time) and sub-second representations (fine time) down to 2-8, 2-16 or 2-24.

The SCET time frame has CAN ID = 0x180 and the following data payload:

**UTC Format**

If the spacecraft provides the optional service of maintaing the UTC on board, the format of the UTC shall be that of the CCSDS day segmented time code(CDS): a 16 bit representation of the number of days elapsed from a predefined epoch (e.g. 1 Jan 2000), 32 bits representing the number of ms of the day and an optional 16 bit field of submilliseconds.

The UTC time frame has CAN ID = 0x280 and the following data payload:

| ![](assets/scet_data_format.png) | ![](assets/utc_data_format.png)
|-|-
| SCET Data Format | UTC Data Format

## Telecommands and Telemetry

**Telecommands**

The controller node can send telecommands to responder nodes. Telecommands usually trigger some kind of action, like switching a unit on or off, changing the mode or configuration of a unit, deploying a solar panel, etc. A telecommand frame contains the node ID of the node to which the telecommand is being sent to (in the CAN header) and the datafield of up to 8 bytes.

**Telemetry** 

The responder nodes send telemetry to the controller node. The sending of telemetry frames from responder nodes is triggered by the controller node either via: a) a specific telecommand that serves as telemetry request, or b) the periodic SYNC frame. Telemetry comprises of essential information about the nodes (also called housekeeping data), such as operational status and sensor readings (temperatures, currents, voltages, etc.). A telemetry frame contains the node ID of the responder node and the datafield of up to 8 bytes.

**Packet Protocol (Optional)**

The 8 bytes of the telecommand and telemetry datafield carry data whose interpretation is in general application-defined.

One possible usage of this datafield is the SpaceCAN packet protocol described in this section. It is defined in close resemblance to the ECSS Packet Utilization Standard [ECSS-E-ST-70-41C](assets/ECSS-E-ST-70-41C.pdf).

First, the packet structure is defined. The 8 bytes of the datafield are structured as follows:

- Byte 1: Service
- Byte 2: Subtype
- Byte 3: Count
- Bytes 4-8: Data

The `service` and `subtype` identify the application service. The `count` is used for packets that contain more than 4 bytes of data. For example, the count would be 0 and 1 for two packets related to an application service that contains 8 bytes.

More details soon...

---

**Implementations**:

- Python (prototype): [https://gitlab.com/librecube/prototypes/python-spacecan](https://gitlab.com/librecube/prototypes/python-spacecan)
- MicroPython: [https://gitlab.com/librecube/lib/micropython-spacecan](https://gitlab.com/librecube/lib/micropython-spacecan)
- mBed C++: [https://gitlab.com/librecube/lib/mbed-spacecan](https://gitlab.com/librecube/lib/mbed-spacecan)
- FreeRTOS (prototype): [https://gitlab.com/librecube/prototypes/freertos-spacecan](https://gitlab.com/librecube/prototypes/freertos-spacecan)
