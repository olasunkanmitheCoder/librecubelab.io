# LibreCube Board Specification

This page defines the normative specification of LibreCube boards. These boards are used in stacked configuration and typically embed system components or entire subsytems (for example, a power system) or larger sensors and actuators. All LibreCube board projects must follow this specification.

The reason for defining such a board specification is to enforce a consistent and modular design of LibreCube elements. Much of this specification is inspired by the typical design of CubeSat boards, which however were never formally defined. This is quite a paradox because the success of the CubeSat program is clearly due to its standardized form factor. The internal electronic boards were never specified by the CubeSat program, which has led to many incompatible CubeSat boards, even among vendors. This shows that it is extremely important to define a formal specification of such boards.

## Mechanical Specification

### Board Layout

- The board shall have a rectangular shape with dimension of 95.89 mm x 90.17 mm and thickness of 1.57 mm (standard PCB FR4 thickness).
- The origin of the coordinate system shall be placed on the bottom left corner with X and Y axis in the directions as shown in Illustration 1, and Z axis in the direction to complete a right-handed coordinate system.
- The board shall have cutouts on its four edges as shown in Illustration 1. Specifically, the cutouts shall span the following rectangles:

  bottom: (36.07, 0) to (56.39, 2.92)

  top: (36.07, 92.97) to (56.39, 95.89)

  left: (0, 37.81) to (2.92, 58.08)
  
  right: (87.25, 37.81) to (90.17, 58.08)

- All corners of the board shall be rounded with a radius of 1.2 mm.
- Four mounting holes with hole diameter of 3.18 mm and a pad diameter of 6.35 mm shall be located at position (5.08, 5.08), (85.09, 5.08), (8.89, 90.81), and (82.55, 90.81).
- A total of 104 pin holes shall be provided, in a grid of 4×26 as shown in Illustration 1. Each hole shall be of diameter 40 mil (1.016 mm) and with a pad of 68 mil (1.7272 mm). The pitch (distance) between pin hole centers shall be 100 mil (2.54 mm). The most bottom left pin center shall be located at (13.97, 83.19).

![](assets/board_outer_dimensions.png)

![](assets/board_cutout_dimensions.png)

### Board Stacking

- Spacing between boards is 15 mm.
- The preferred side to mount components is the top side (A).
- Components shall not exceed a height of 8.76 mm above the top side (A).
- Components shall not exceed a height of 4.83 mm below the bottom side (B).

![](assets/board_stacking.png)

### Components

- Connector: [SAMTEC](https://wwws.samtec.com/) ESQ-126-39-[P]-D and ESQ-126-39-[P]-S or compatibles. The plating option [P] may be either gold or tin.
- Standoff: 15mm hex standoff aluminum, for example from [here](http://www.digikey.nl/short/3z3tjw).

| ![](assets/board_connector.png) | ![](assets/board_standoff.png)
|-|-
|Board Connector|Board Standoff

## Electrical Specification

The naming of the headers (H1 and H2) and the pin numbering is:

![](assets/connector_headers.png)

Spreadsheet: [https://docs.google.com/spreadsheets/d/1N1JiXR-5huo--XefjsvC9CI1hiS9xMNosTZhUcuxiQ0/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1N1JiXR-5huo--XefjsvC9CI1hiS9xMNosTZhUcuxiQ0/edit?usp=sharing)

**H1**

|Pin  |Name       |Description
|-|-|-
|1    |CAN_A_L    |Nominal CAN Bus (Low Signal)
|3    |CAN_A_H    |Nominal CAN Bus (High Signal)
|31-32|CHARGE     |Battery charge input (5 Volt)
|33-34|GND        |Ground

**H2**

|Pin  |Name       |Description
|-|-|-
|1 	  |CAN_B_L 	  |Redundant CAN Bus (Low Signal)
|3 	  |CAN_B_H 	  |Redundant CAN Bus (High Signal)
|25-26|5V         |Main Bus Power
|29-32|GND 	      |Ground
|45-46|VBAT       |Direct Battery Output

## Communication Specification

### System Bus

CubeSats are often designed as highly distributed architectures, where microcontrollers are located at each subsytem to implement local autonomous acquisition and control. Still, a central node (the on-board computer) is needed to orchestrate the overall activities and status of the satellite. The system bus is the backbone for such on-board command and data exchange and must therefore be reliable and robust against failures. Therefore, SpaceCAN was developed as system bus for LibreCube projects.

(Other dedicated bus systems (for example for payload data transfer) are to be defined)

---

**Template**

https://gitlab.com/librecube/templates/librecube-board

**References**

- [PC/104 Specification](https://pc104.org/wp-content/uploads/2015/02/PC104_Spec_v2_6.pdf)
- [PC/104-Plus Specification](https://pc104.org/wp-content/uploads/2015/02/PC104_Plus_v2_32.pdf)
- [CubeSat Design Specification](https://www.cubesat.org/resources/)
