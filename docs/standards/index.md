# Standards

Standardization is a really important aspect of any engineering field. Just think
of the USB standard, which makes it possible to plug many different kind of devices
into your laptop! An example of the lack of a unified standards are power plugs;
you will need to bring along different adapters to be able to charge your
laptop when traveling in other countries - what a mess!

In general, the use of standards helps to reduce costs and ensure compatability.
The standardization of the outer dimensions of CubeSats (in particular the deployer)
has led to the success of the CubeSat program. Thus it must seem obvious that
further standardization of satellites can lead to more benefits.

For LibreCube we are promoting the application of standards wherever possible.

## Public Standards

Unfortunately, one of the main organization bodies, the ISO, charges a huge fee
for their standards. Luckely for the space domain there are organizations that
publish dedicated standards for application in space projects, freely and openly.

Those organizations are the [European Cooperation for Space Standardization (ECSS)](https://ecss.nl/)
and the [Consultative Committee for Space Data Systems (CCSDS)](https://public.ccsds.org).

Be warned however, those standards can be hard to read. Also, there are so many
of them (several dozends) which may make it hard to find the right one for your case.

Therefore, please have a look at this handbook that provides a summary overview
of most relevant CCSDS and ECSS standards for LibreCube's purposes:

[Handbook of Free and Open Space Standards](assets/Handbook_of_Free_and_Open_Space_Standards.pdf)

If you like to contribute to this handbook, have a look at the
[source repository](https://gitlab.com/artur-scholz/handbook-space-standards).

## LibreCube Standards

When there is no public standard available for specific cases, the LibreCube community
develops dedicated standards applicable for their projects. However, we intend to keep
this effort this to a minimum. The first step after the need for standardization has been identified
is to set up a working group that reviews existing solutions and how those
can be harmonized or tailored.

The standards developed by LibreCube are applicable to all LibreCube projects
and may be used freely for any other projects as well.

The following standards have been developed:

- [SpaceCAN](spacecan/index.md)
- [LibreCube Board Specification](board_specification/index.md)
